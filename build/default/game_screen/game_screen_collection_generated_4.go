embedded_components {
  id: "coins_factory"
  type: "factory"
  data: "prototype: \"/game_screen/factory_objects/coins.go\"\nload_dynamically: false\n"
  position {
    x: 0.0
    y: 0.0
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
embedded_components {
  id: "nitros_factory"
  type: "factory"
  data: "prototype: \"/game_screen/factory_objects/nitros.go\"\nload_dynamically: false\n"
  position {
    x: 0.0
    y: 0.0
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
embedded_components {
  id: "stars_factory"
  type: "factory"
  data: "prototype: \"/game_screen/factory_objects/star.go\"\nload_dynamically: false\n"
  position {
    x: 0.0
    y: 0.0
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
embedded_components {
  id: "count_factory"
  type: "factory"
  data: "prototype: \"/game_screen/factory_objects/count.go\"\nload_dynamically: false\n"
  position {
    x: 0.0
    y: 0.0
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
