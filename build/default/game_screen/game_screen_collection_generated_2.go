embedded_components {
  id: "bg"
  type: "sprite"
  data: "tile_set: \"/game_screen/game_screen_atlas.atlas\"\ndefault_animation: \"background\"\nmaterial: \"/builtins/materials/sprite.material\"\nblend_mode: BLEND_MODE_ALPHA\n"
  position {
    x: 0.0
    y: 0.0
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
embedded_components {
  id: "road"
  type: "sprite"
  data: "tile_set: \"/game_screen/game_screen_atlas.atlas\"\ndefault_animation: \"road\"\nmaterial: \"/builtins/materials/sprite.material\"\nblend_mode: BLEND_MODE_ALPHA\n"
  position {
    x: 16.363
    y: 0.0
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
embedded_components {
  id: "trees_factory"
  type: "factory"
  data: "prototype: \"/game_screen/factory_objects/trees.go\"\nload_dynamically: false\n"
  position {
    x: 0.0
    y: 0.0
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
embedded_components {
  id: "road_lines_factory"
  type: "factory"
  data: "prototype: \"/game_screen/factory_objects/road_line.go\"\nload_dynamically: false\n"
  position {
    x: 0.0
    y: 0.0
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
