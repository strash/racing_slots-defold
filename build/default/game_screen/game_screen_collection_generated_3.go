embedded_components {
  id: "sprite"
  type: "sprite"
  data: "tile_set: \"/game_screen/game_screen_atlas.atlas\"\ndefault_animation: \"car_fart\"\nmaterial: \"/builtins/materials/sprite.material\"\nblend_mode: BLEND_MODE_ALPHA\n"
  position {
    x: -14.0
    y: -110.0
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
embedded_components {
  id: "sprite1"
  type: "sprite"
  data: "tile_set: \"/game_screen/game_screen_atlas.atlas\"\ndefault_animation: \"car_fart\"\nmaterial: \"/builtins/materials/sprite.material\"\nblend_mode: BLEND_MODE_ALPHA\n"
  position {
    x: 58.0
    y: -110.0
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
