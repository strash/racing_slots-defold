components {
  id: "main_controller"
  component: "/main/main_controller.script"
  position {
    x: 0.0
    y: 0.0
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
embedded_components {
  id: "start_screen_proxy"
  type: "collectionproxy"
  data: "collection: \"/start_screen/start_screen_collection.collection\"\nexclude: false\n"
  position {
    x: 0.0
    y: 0.0
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
embedded_components {
  id: "game_screen_proxy"
  type: "collectionproxy"
  data: "collection: \"/game_screen/game_screen_collection.collection\"\nexclude: false\n"
  position {
    x: 0.0
    y: 0.0
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
